CiviCRM Subscribe Module

This module attempts to provide a compatability layer between the old CiviCRM
1.0 API and the new CiviCRM 2.0 API.

Only install this module if you are using CiviCRM 2.0 and if a CiviCRM module
depends on it, or the CiviCRM module project page tells you to install this
module.

ATTENTION MODULE DEVELOPERS:

1.  This is an API module that requires (minimal) change to your civicrm
dependent module.

The only change you need to make in your code is to replace calls to
civicrm_initialize() to civicrm20_initialize().

But it is reocommended that you do something like:

function yourmodule_civicrm_initialize() {
  if (function_exists('civicrm20_initialize')) {
    civicrm20_initialize(true);
  }
  else {
    civicrm_initialize(true);
  }
}

And then replace all calls to civicrm_initialize() with calls to
yourmodule_civicrm_initialize().

2.  New modules should be written using the new CiviCRM 2.0 API.  But this file
provides a quick way to upgrade to CiviCRM 2.0 without a lot of work.
